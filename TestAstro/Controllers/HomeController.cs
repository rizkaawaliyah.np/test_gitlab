﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TestAstro.Models;

namespace TestAstro.Controllers
{
    public class HomeController : Controller
    {
        db_project db_project = new db_project();

        public ActionResult Index()
        {
            if(!User.Identity.IsAuthenticated || !User.IsInRole("admin"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "tbl_user");
            }
        }

        [Route("Register")]
        public ActionResult Register()
        {
            return View();
        }

        [Route("Register")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(tbl_user registerUser)
        {
            using (var transaction = db_project.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        registerUser.badgeid = 1;
                        registerUser.point = 0;
                        registerUser.photo = "default";
                        tbl_role_mapping u_role = new tbl_role_mapping();
                        u_role.roleid = 1;
                        u_role.userid = registerUser.id;
                        db_project.tbl_role_mapping.Add(u_role);
                        db_project.tbl_user.Add(registerUser);
                        db_project.SaveChanges();
                        FormsAuthentication.SetAuthCookie(registerUser.username, false);
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    return View();
                }

                catch (Exception error)
                {
                    transaction.Rollback();
                    ErrorLog("Visitor", error.Message, "Register");
                    ViewBag.error = error.Message;
                    return View();
                }
            }
                
            
        }

        [Route("Login")]
        public ActionResult Login()
        {
            return View();
        }

        [Route("Login")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(tbl_user user)
        {
            if (ModelState.IsValid)
            {
                bool IsValidUser = db_project.tbl_user
               .Any(u => u.username.ToLower() == user.username.ToLower() && u.password == user.password);

                if (IsValidUser)
                {
                    FormsAuthentication.SetAuthCookie(user.username, false);
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError("", "invalid Username or Password");
            return View();
        }

        [Route("Logout")]

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private void ErrorLog(string username, string message, string location)
        {
            db_project db_error = new db_project();
            tbl_error_log_history error = new tbl_error_log_history();
            error.username = username;
            error.err_msg = message;
            error.err_loc = location;
            error.err_date = DateTime.Now;
            db_error.tbl_error_log_history.Add(error);
            db_error.SaveChanges();
        }

        [Route("News")]
        public ActionResult News()
        {
            return View();
        }

        [Route("Events")]
        public ActionResult Events()
        {
            return View();
        }

        [Route("Discussions")]
        public ActionResult Discussions()
        {
            return View();
        }

        [Route("Courses")]
        public ActionResult Courses()
        {
            return View();
        }
    }
}